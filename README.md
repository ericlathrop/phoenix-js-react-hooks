# phoenix-js-react-hooks

[React](https://reactjs.org/) [hooks](https://reactjs.org/docs/hooks-reference.html) for working with [Phoenix's](https://www.phoenixframework.org/) [channels](https://hexdocs.pm/phoenix/channels.html).

## Usage

Wrap the top of your app with a `<SocketProvider>` so child components will have
access to the socket.

```javascript
import { SocketProvider } from '@ericlathrop/phoenix-js-react-hooks';

function App() {
  const socketUrl = "wss://localhost:4000/socket";
  const socketOptions = { params: { 'user_id': userId }};

  return (
    <SocketProvider url={socketUrl} options={socketOptions}>
      <Main />
    </SocketProvider>
  );
}
```

The `useChannel` hook lets you subscribe to a channel, and the `useEventHandler`
hook lets you handle incoming messages:

```javascript
import React, { useContext, useEffect, useRef, useState } from 'react';
import { sendMessage, useChannel, useEventHandler } from '@ericlathrop/phoenix-js-react-hooks';

export function Main() {
  const [messages, setMessages] = useState([]);

  const room = 'cute kitties';
  const { channel: chatChannel } = useChannel('chat:' + room, undefined, (channel, { messages: initialMessages}) => {
    setMessages(initialMessages);
  });

  useEventHandler(chatChannel, 'new_message', message => {
    setMessages(messages.slice(0).concat([message]));
  });

  return (
    <div className="chat-messages" >
      {messages.map(({ id, message }) => <ChatMessage key={id} message={message} />)}
    </div>
  );
}
```

### <SocketProvider url={url} options={options}>

A React [Context.Provider](https://reactjs.org/docs/context.html#contextprovider) that provides a `socket` value which is the Phoenix [Socket](https://hexdocs.pm/phoenix/js/index.html#socket) object.

The `url` prop is passed as the `endPoint` argument to the `Socket` constructor, and the
`options` prop is passed as the `opts` argument to the `Socket` constructor.

### const channel = useChannel(topic, params, onJoin)

A React [hook](https://reactjs.org/docs/hooks-intro.html) that connects to a
Phoenix [Channel](https://hexdocs.pm/phoenix/js/index.html#channel) and lets you
send/receive events. `useChannel` returns the Phoenix Channel that you can use
in `useEventHandler` or `sendMessage`.

The `topic` argument is passed as the `topic argument to the `Channel
constructor, and the `params` argument is passed as the `params` argument to the
`Channel` constructor. The `onJoin(channel, message)` argument is a function that is called when
the channel is successfully joined.

### useEventHandler(channel, event, handler)

A React [hook](https://reactjs.org/docs/hooks-intro.html) that specifies an
event handler function to to executed when an event is received.

The `channel` argument is the Phoenix [Channel](https://hexdocs.pm/phoenix/js/index.html#channel) on which to listen for events.

The `event` argument is a string specifying the name of the event to listen for.

The `handler(message3)` argument is a function that will be called with the
message every time a matching event is received.

### const promise = sendMessage(channel, event, payload)

A function that sends an event on a Phoenix [Channel](https://hexdocs.pm/phoenix/js/index.html#channel). `sendMessage` returns a [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves to the reply message from the server, if any.

The `channel` argument is the Phoenix [Channel](https://hexdocs.pm/phoenix/js/index.html#channel) on which to send the event.

The `event` argument is a string specifying the name of the event to send.

The `payload` argument is a JavaScript object containing the data for the event
you are sending.
